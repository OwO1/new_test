#!/bin/sh

# migrate
export PYTHONPATH=`pwd`
export FLASK_APP=app.py
flask db upgrade

# start gunicorn
exec gunicorn -w 3 app:app -n app -b :9090 --access-logfile - --access-logformat '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" %(L)s'
