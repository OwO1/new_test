FROM ubuntu:18.04

COPY sources.list /etc/apt/sources.list
RUN apt update
RUN apt -y install python3 python3-pip

# args and envs
ARG PYPI_AUTH
ARG PROJECT_PATH=/test
ENV PYTHONPATH=$PROJECT_PATH
ENV FLASK_ENV=production

# pwd
RUN mkdir $PROJECT_PATH
WORKDIR $PROJECT_PATH

# entrypoint and requirements
COPY docker-entrypoint.sh docker-entrypoint.sh
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt -i https://pypi.douban.com/simple
RUN chmod +x docker-entrypoint.sh

# source code
COPY app.py app.py


EXPOSE 9090
CMD ["./docker-entrypoint.sh"]
